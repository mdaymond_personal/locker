import * as express from 'express'
import { Application } from 'express'
import {initApi} from './api/apiRoutes'
import {APIErrorHandler} from './api/APIErrorHandler'
const bodyParser = require ('body-parser')

const app: Application = express()

//Body parser is a middleware for express.   This give express the capability to parse incoming request
// with json payload
app.use(bodyParser.json())

initApi(app)

app.use(APIErrorHandler)

app.listen(8090, () => {
    console.log("server running")
})

//Restructure project
//http://www.codekitchen.ca/guide-to-structuring-and-building-a-restful-api-using-express-4/

