import {Response} from 'express'
const hri = require('human-readable-ids').hri  //No Types

export function databaseErrorHandler(res:Response, err:any){
//Provide an id to be found in the logs so that error information can be found in the logs 
            //and not sent back to the client.
            const id = hri.random()
            console.log("Database Error" ,id, err)
            //What we send back to the client.
            res.status(500).json({
                code: "ERR-002",
                message: `Error creating item.  Log identifier: ${id}`
            })
}
