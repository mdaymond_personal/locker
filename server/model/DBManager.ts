import {ProductModel, ItemModel }  from "../model/Models"

export function findAllProducts(){
    return ProductModel.findAll({
    order: ['sort_order']
})
}
export function getProductDetail(id:number){
    return ProductModel.findById(id,{
        include:[
            {
                model: ItemModel
            }
        ]
    })
}

export function createItem(props:any){
    return ItemModel.create(props)
}

export function updateItem(id:string, props:any){
    return ItemModel.update(
        props,
        {
            where:{id:id}  //can use where:{id} to shorten
        }
    )
}

export function deleteItem(id:string){
    return ItemModel.destroy(
        {
            where:{id:id}
        }
    )
}




