import * as ORM from "Sequelize"
import { Sequelize, LoggingOptions } from "Sequelize"  //Required to get the sequalize autocompelete types

export function initProductAttributes(sequelize: Sequelize) {
    return sequelize.define('products', {
        name: ORM.STRING,
        product_type: ORM.STRING,
        description: ORM.TEXT,
        short_description: ORM.TEXT,
        cost: ORM.INTEGER,
        featured: ORM.BOOLEAN,
        visible: ORM.BOOLEAN
    },
        { timestamps: false }
    )
}