import * as ORM from "Sequelize"
import { Sequelize, LoggingOptions } from "Sequelize"  //Required to get the sequalize autocompelete types
import {initProductAttributes}  from "./ProductAttrributes"
import {initItemAttributes} from "./ItemAttributes"

const dbUrl = 'postgresql://localhost:5432/zenlocker_development'

const options:LoggingOptions = {benchmark:true, logging:console.log}
const sequelize: Sequelize = new ORM(dbUrl, options);

//Export the model
export const ProductModel = initProductAttributes(sequelize)
export const ItemModel = initItemAttributes(sequelize)

ProductModel.hasMany(ItemModel,{
    foreignKey: "product_id"
})

ItemModel.belongsTo(ProductModel,{
    foreignKey: "product_id"
})