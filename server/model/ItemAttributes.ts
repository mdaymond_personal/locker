import * as ORM from 'sequelize'
import { Sequelize } from 'sequelize'

export function initItemAttributes(sequelize: Sequelize) {
    return sequelize.define('items', {
        name: ORM.STRING,
        description: ORM.TEXT,
        status: ORM.STRING
    },
        { timestamps: false }
    )
}