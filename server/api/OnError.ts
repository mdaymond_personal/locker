import {Response} from 'express'

export function OnError(res:Response, message:string, err:any){
    console.error("Error in promise chain", message, err)
    res.status(500).send()
}