import { Application } from 'express'
import * as apiManager from './apiManager'

export function initApi(app: Application) {
    console.log("API init")
    app.route('/api/products').get(apiManager.findAllProducts)
    app.route('/api/products/:id').get(apiManager.getProductDetail)
    
    app.route('/api/item/create').post(apiManager.createItem)
    app.route('/api/item/update/:id').patch(apiManager.updateItem) //make incremental update
    app.route('/api/item/delete/:id').patch(apiManager.deleteItem) //make incremental update
}