import {Response} from 'express'

export function OnSuccess(res:Response, data :any){
    res.status(200).json({
        payload:data
    })
}
