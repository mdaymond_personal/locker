import {RequestHandler, ErrorRequestHandler, Request, Response, NextFunction} from 'express'

//This avoids a stacktrace being thrown to the browser.  Doing so will be a security vulnerability.

export function APIErrorHandler(err:any, req:Request, res:Response, next:NextFunction){
    console.error("The error handler: ", err )
    res.status(500).json({errorCode: 'ERR-501', message: "Internal Server Error"})  //Returned to customer
}

