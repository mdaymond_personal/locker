import * as db from '../model/DBManager'
import { Request, Response } from 'express'
import {OnError} from './OnError'
import {databaseErrorHandler} from '../model/DBErrorHandler'
import {OnSuccess} from './OnSuccess'
import * as _ from 'lodash'

export function findAllProducts(req: Request, res: Response) {

    db.findAllProducts() //returns promise
        //Could have an issue here in the promise chane.  this will create a hung browser.
        //Need to handle errors in the promise chain
        //.then(() => {throw new Error ("Hung")})
        //.then expects a function containing one argument.  So using lodash partial to pass additional args to the OnSuccess
        .then(_.partial(OnSuccess, res)) //When called data will be automatically passed in.

        //Catches an error in the promise chain.
        //Option1 to record the error this way... 
            //.catch (err => OnError(res, "findAllProducts failure", err))
        //Option2 with loadash to .  For partial functions
        .catch(_.partial(OnError, res, "findAllProducts failure"))
}

export function getProductDetail(req:Request, res:Response){
    const productId = parseInt(req.params.id)
    db.getProductDetail(productId)
        .then(_.partial(OnSuccess, res))
        .catch(_.partial(OnError, res, "getProductDetail failure"))

}

export function createItem(req:Request, res:Response){
    const props = req.body
    db.createItem(props)
        .then(_.partial(OnSuccess, res))        
        .catch(_.partial(databaseErrorHandler,res))
        .catch(_.partial(OnError, res, "createItem failure"))
}

export function updateItem(req:Request, res:Response){
    const itemId = req.params.id
    db.updateItem(itemId, req.body)
        .then(_.partial(OnSuccess, res))
        .catch(_.partial(databaseErrorHandler,res))
        .catch(_.partial(OnError, res, "updateItem failure"))

}

export function deleteItem(req:Request, res:Response){
    const itemId = req.params.id
    db.deleteItem(itemId)
        .then(_.partial(OnSuccess, res))
        .catch(_.partial(databaseErrorHandler,res))
        .catch(_.partial(OnError, res, "deleteItem failure"))
    
}


